# -*- coding: UTF-8 -*-
import os
import shutil
import zipfile

from invoke import task


@task
def unzip(c, pathfile):
    if pathfile:
        path = os.path.splitext(pathfile)[0]
        if os.path.exists(path):
            shutil.rmtree(path)

        # Extract zip
        with zipfile.ZipFile(pathfile, "r") as zip_ref:
            zip_ref.extractall(path)


@task
def convert(c, pathfolder, columns):
    # Convert nginx log to csv format
    for top, dirs, files in os.walk(pathfolder):
        for nm in files:
            try:
                print 'Convert :', os.path.join(top, nm)
                pathlog = os.path.join(top, nm)

                if pathlog.endswith(".csv"):
                    continue
                else:
                    pathcsv = pathlog + '.csv'
                    with open(pathlog) as file_a:

                        # Add columns csv
                        set_columns = ''
                        arr_columns = [i for i in columns.split(',')]
                        for i in range(len(arr_columns)):
                            if i != len(arr_columns) - 1:
                                set_columns += '"%s"' % arr_columns[i] + ','
                            else:
                                set_columns += '"%s"' % arr_columns[i]
                        array = [set_columns]

                        array += ['"' + row.strip().replace('\t', '","') + '"' for row in file_a]
                    with open(pathcsv, 'wb') as file_b:
                        for line in array:
                            file_b.write(line + '\n')
            except OSError:
                pass