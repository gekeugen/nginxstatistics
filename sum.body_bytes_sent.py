# -*- coding: utf-8 -*-
import operator
import pandas as pd
import matplotlib.pyplot as plt

path = '/home/kheo/WIC/qwicservices.access.log.10.csv'
df = pd.read_csv(path, sep=',')
tuples = [tuple(x) for x in df.values]
NOTSERVISES = ['', ' HTTP', 'errors HTTP', 'favicon.ico HTTP', 'errors', 'files']

# Уникальный сисок сервисов
def get_list_services():
    arr_name = []
    for x in range(len(tuples)):
        if tuples[x][4].split('/', 2)[1] not in NOTSERVISES:
            arr_name.append(tuples[x][4].split('/', 2)[1])
    return list(set(arr_name))

arr_servises = get_list_services()

# сумма значений body_bytes_sent для каждого сервиса
def get_values_services(servises):
    arr_value=[]
    for servise in servises:
        arr_values=[]
        for x in range(len(tuples)):
            if tuples[x][4].split('/', 2)[1]  == servise:
                arr_values.append(tuples[x][6])
        arr_value.append(sum(arr_values)/(1024**2))
    return arr_value

arr_values = get_values_services(arr_servises)
#body_bytes_sent name service
service_and_value = dict(zip(arr_servises, arr_values))
# Сортировка по позрастанию словаря
sorted_x = sorted(service_and_value.items(), key=operator.itemgetter(1))

# сумма значений upstream_response_time для каждого сервиса
def get_values_services_urt(servises):
    arr_value=[]
    for servise in servises:
        arr_values=[]
        for x in range(len(tuples)):
            if tuples[x][4].split('/', 2)[1]  == servise:
                if (str(tuples[x][10]) != '-'):
                    # 3 нуля пс запятой
                    result = sum([float(item) for item in tuples[x][10].replace(' ','').split(',')])
                    arr_values.append(result)
        arr_value.append(sum(arr_values))
    return arr_value

arr_values_urt = get_values_services_urt(arr_servises)
#body_bytes_sent name service
service_and_value_urt = dict(zip(arr_servises, arr_values_urt))
sorted_urt = sorted(service_and_value_urt.items(), key=operator.itemgetter(1))

# Вывод
for i in sorted_x:
    print i[0], ':', i[1], 'Mb'
print ('\n')


# Ввод значений границ
def inputlimit():
    #print("Input limit ")
    limit_lower, limit_upper = None, None
    while type(limit_lower and limit_upper) != "int":
        try:
            limit_lower = int(input("Input lower limit value :"))
            limit_upper = int(input("Input upper limit value :"))
            if ((limit_lower >= 0) and  (limit_upper > 0)) and (limit_lower < limit_upper) \
                    and (limit_upper <= max([i[1] for i in sorted_x])):
                break
            else:
                print ('Input: ',\
                    min([i[1] for i in sorted_x]),'..' ,max([i[1] for i in sorted_x]))
        except ValueError:
            print("Not's integer!")

    return limit_lower, limit_upper

limit_ = inputlimit()
limit_lower = limit_[0]
limit_upper = limit_[1]

# количество методов для каждого сервиса
def get_values_methods_services(servises):
    arr_value=[]
    for servise in servises:
        count = 0
        for x in range(len(tuples)):
            if (' /%s/' % (servise)) in tuples[x][4]:
                count += 1
        arr_value.append(count)
    return arr_value


arr_values_methods = get_values_methods_services(arr_servises)
service_and_meth = dict(zip(arr_servises, arr_values_methods))


# Фильтрация
arr_serv_name, arr_serv_value, arr_meth = [], [],[]
for servise in arr_servises:
    if limit_lower <= service_and_value[servise] <= limit_upper:
        arr_serv_name.append(servise)
        arr_serv_value.append(service_and_value[servise])
        arr_meth.append(service_and_meth[servise])


def make_autopct(values,type):
    def my_autopct(pct):
        total = sum(values)
        val = int(round(pct*total/100.0))
        return '{v:d}  {t}'.format(v=val,t=type)
    return my_autopct

plt.figure(figsize=(10,10))
plt.title('Sum. body_bytes_sent %s..%s Mb File : %s' % (limit_lower, limit_upper, path.split('/')[-1]), fontsize=20)
plt.pie(arr_serv_value,labels= arr_serv_name,autopct = make_autopct(arr_serv_value,'Mb'))
#plt.savefig("Sum. body_bytes_sent %s..%s.pdf" % (limit_lower, limit_upper), bbox_inches='tight')


plt.figure(figsize=(10,10))
plt.title('Count  Methods %s..%s Mb  File : %s' % (limit_lower, limit_upper, path.split('/')[-1]), fontsize=20)
plt.pie(arr_meth,labels= arr_serv_name,autopct = make_autopct(arr_meth,''))
#plt.savefig("Count  Methods %s..%s.pdf" % (limit_lower, limit_upper), bbox_inches='tight')
plt.show()
