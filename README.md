### Начальная установка
    pip install -r requirements.txt
    invoke unzip --pathfile data.zip 
    invoke convert --pathfolder data --columns 'remote_addr','remote_user','time_iso8601','upstream_addr','request','status','body_bytes_sent','request_length','http_user_agent','request_time','upstream_response_time','remote_port','connection','connection_requests','server_port'

### nginx лог

###### 0 remote_addr - адрес клиента
###### 1 remote_user - имя пользователя, использованное в Basic аутентификации
###### 2 time_iso8601 - локальное время в формате по стандарту ISO 8601
###### 3 upstream_addr - хранит IP-адрес и порт
###### 4 request - запрос
###### 5 status - статус ответа
###### 6 body_bytes_sent - [bytes] число байт, переданное клиенту, без учёта заголовка ответа
###### 7 request_length - длина запроса (включая строку запроса, заголовок и тело запроса)
###### 8 http_user_agent
###### 9 request_time - [sec] время обработки запроса в секундах с точностью до миллисекунд; время, прошедшее с момента чтения первых байт от клиента
###### 10 upstream_response_time - [sec] время между установлением соединения с вышестоящим сервером и получением первого байта заголовка ответа
###### 11 remote_port - порт клиента
###### 12 connection - доступное количество коннектов
###### 13 connection_requests - текущее число запросов в соединении
###### 14 server_port - порт сервера, принявшего запрос


### Вывод sum.body_bytes_sent
![alt text](https://i.imgur.com/fUnppvU.png)

![alt text](https://i.imgur.com/JKjLjw2.png)

![alt text](https://i.imgur.com/ncP6Uqr.png)


### Вывод upstream_response_time
